package ru.ravikwow.bpe.presentation.vm;

import androidx.databinding.Observable;
import androidx.databinding.ObservableField;
import ru.ravikwow.bpe.common_domain_presentation.ItemM;

/**
 * Created by ravikwow
 * Date: 19.11.18.
 */
public class ItemVM {
    public final ObservableField<String> key = new ObservableField<>();
    public final ObservableField<String> value = new ObservableField<>();
    private ItemM itemM;
    private final Observable.OnPropertyChangedCallback changedCallback = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            if (sender == key) {
                itemM.setKey(key.get());
            } else if (sender == value) {
                itemM.setValue(value.get());
            }
        }
    };

    public ItemVM() {
        key.addOnPropertyChangedCallback(changedCallback);
        value.addOnPropertyChangedCallback(changedCallback);
    }

    public ItemM getItemM() {
        return itemM;
    }

    public void bind(ItemM itemM) {
        this.itemM = itemM;
        key.set(itemM.getKey());
        value.set(itemM.getValue());
    }
}
