package ru.ravikwow.bpe.presentation.ui;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import ru.ravikwow.bpe.presentation.BR;
import ru.ravikwow.bpe.presentation.BuildConfig;
import ru.ravikwow.bpe.presentation.R;
import ru.ravikwow.bpe.presentation.adapter.ItemsAdapter;
import ru.ravikwow.bpe.presentation.utils.KeyboardUtils;
import ru.ravikwow.bpe.presentation.vm.ItemVM;
import ru.ravikwow.bpe.presentation.vm.MainActivityVM;
import ru.ravikwow.bpe.presentation.vm.factory.MainActivityVMFactory;
import ru.ravikwow.bpe.presentation.widget.WrapContentLinearLayoutManager;

public class MainActivity extends AppCompatActivity {

    private ItemsAdapter mAdapter = new ItemsAdapter();
    private MainActivityVM vm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.app_name_full);
        ViewDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        vm = ViewModelProviders.of(this, new MainActivityVMFactory(getApplication(), mAdapter)).get(MainActivityVM.class);
        binding.setVariable(BR.viewModel, vm);
        vm.getData().observe(this, items -> mAdapter.setItems(items));
        vm.getShowEditDialog().observe(this, this::showEditDialog);
        vm.getShowAddDialog().observe(this, o -> showAddDialog());
        vm.getShowAboutDialog().observe(this, o -> showAboutDialog());

        initList();
    }

    private void initList() {
        RecyclerView rv = findViewById(R.id.rv);
        rv.setLayoutManager(new WrapContentLinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        rv.clearOnScrollListeners();
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                FloatingActionButton fab = findViewById(R.id.fab_add);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        fab.show();
                        break;
                    default:
                        fab.hide();
                        break;
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (vm.onContextItemSelected(item)) {
            return true;
        } else {
            return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return vm.search(newText);
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (vm.onMenuItemSelected(item)) {
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void showAddDialog() {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(this).inflate(R.layout.dialod_view_add, null, false);
        AppCompatEditText keyEditText = view.findViewById(R.id.key_edit_text);
        AppCompatEditText valueEditText = view.findViewById(R.id.value_edit_text);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.add)
                .setPositiveButton(android.R.string.ok, (dialog1, which) -> vm.set(keyEditText.getText().toString(), valueEditText.getText().toString()))
                .setNegativeButton(android.R.string.cancel, null)
                .create();
        dialog.setView(view);
        KeyboardUtils.requestInputMethod(dialog);
        dialog.show();
    }

    private void showEditDialog(ItemVM itemVM) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(this).inflate(R.layout.dialod_view_edit, null, false);
        AppCompatEditText valueEditText = view.findViewById(R.id.value_edit_text);
        valueEditText.setText(itemVM.value.get());
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(itemVM.key.get())
                .setPositiveButton(android.R.string.ok, (dialog1, which) ->
                        vm.onItemChange(itemVM, valueEditText.getText().toString()))
                .setNegativeButton(android.R.string.cancel, null)
                .create();
        dialog.setView(view);
        KeyboardUtils.requestInputMethod(dialog);
        dialog.show();
    }

    private void showAboutDialog() {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(this).inflate(R.layout.dialod_view_about, null, false);
        AppCompatTextView versionView = view.findViewById(R.id.version);
        AppCompatTextView authorView = view.findViewById(R.id.author);
        String versionName = "xxx";
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int versionCode = 0;
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionView.setText(String.format(getResources().getString(R.string.version), versionName, versionCode));
        Date buildDate = new Date(BuildConfig.BUILD_DATE);
        DateFormat dateFormat = new SimpleDateFormat("yyyy", getResources().getConfiguration().locale);
        authorView.setText(String.format(getResources().getString(R.string.author), dateFormat.format(buildDate)));
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.about)
                .setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss())
                .create();
        dialog.setView(view);
        dialog.show();
    }
}
