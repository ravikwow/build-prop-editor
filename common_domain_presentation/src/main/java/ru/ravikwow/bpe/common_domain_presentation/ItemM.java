package ru.ravikwow.bpe.common_domain_presentation;

import java.util.Arrays;

public class ItemM {
    private String key;
    private String value;

    public ItemM() {
    }

    public ItemM(String key, String value) {

        this.key = key;
        this.value = value;
    }

    private static boolean equals(Object var0, Object var1) {
        return var0 == var1 || var0 != null && var0.equals(var1);
    }

    private static int hash(Object... var0) {
        return Arrays.hashCode(var0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemM itemM = (ItemM) o;
        return equals(key, itemM.key) &&
                equals(value, itemM.value);
    }

    @Override
    public int hashCode() {
        return hash(key, value);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
