package ru.ravikwow.bpe.presentation.vm.factory;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import ru.ravikwow.bpe.presentation.adapter.ItemsAdapter;
import ru.ravikwow.bpe.presentation.vm.MainActivityVM;

/**
 * Created by ravikwow
 * Date: 21.11.18.
 */
public class MainActivityVMFactory extends ViewModelProvider.NewInstanceFactory {
    private ItemsAdapter adapter;
    private Application application;

    public MainActivityVMFactory(@NonNull Application application, ItemsAdapter adapter) {
        this.application = application;
        this.adapter = adapter;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (AndroidViewModel.class.isAssignableFrom(modelClass)) {
            //noinspection unchecked
            return (T) new MainActivityVM(application, adapter);
        }
        return super.create(modelClass);
    }
}
