package ru.ravikwow.bpe.presentation.utils;

import android.app.Dialog;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created: ravikwow
 * Date: 25.12.15
 */
public class KeyboardUtils {
    public static void requestInputMethod(Dialog dialog) {
        Window window = dialog.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
}
