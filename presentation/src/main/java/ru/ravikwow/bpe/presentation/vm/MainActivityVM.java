package ru.ravikwow.bpe.presentation.vm;

import android.app.Application;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import ru.ravikwow.bpe.common_data_domain.Repository;
import ru.ravikwow.bpe.common_domain_presentation.ItemM;
import ru.ravikwow.bpe.data.RepositoryImpl;
import ru.ravikwow.bpe.domain.ItemsUseCase;
import ru.ravikwow.bpe.presentation.R;
import ru.ravikwow.bpe.presentation.adapter.ItemsAdapter;
import ru.ravikwow.bpe.presentation.architecture.SingleLiveEvent;

/**
 * Created by ravikwow
 * Date: 21.11.18.
 */
public class MainActivityVM extends AndroidViewModel implements View.OnCreateContextMenuListener {
    public final ObservableField<ItemsAdapter> adapter = new ObservableField<>();
    public final ObservableBoolean isHaveRoot = new ObservableBoolean(false);
    private final ItemsUseCase itemsUseCase;
    private MutableLiveData<List<ItemM>> data;
    private SingleLiveEvent<ItemVM> showEditDialog = new SingleLiveEvent<>();
    private SingleLiveEvent<Object> showAddDialog = new SingleLiveEvent<>();
    public final View.OnClickListener onClickAdd = v -> showAddDialog.call();
    private SingleLiveEvent<Object> showAboutDialog = new SingleLiveEvent<>();

    public MainActivityVM(@NonNull Application application, ItemsAdapter adapter) {
        super(application);
        Repository repository = new RepositoryImpl(application);
        itemsUseCase = new ItemsUseCase(repository);
        this.adapter.set(adapter);
        adapter.setOnClickListener(this::onItemClick);
        adapter.setOnItemCreateContextMenu(this);
    }

    public MutableLiveData<Object> getShowAboutDialog() {
        return showAboutDialog;
    }

    public MutableLiveData<Object> getShowAddDialog() {
        return showAddDialog;
    }

    public MutableLiveData<ItemVM> getShowEditDialog() {
        return showEditDialog;
    }

    public void set(String key, String value) {
        key = key.replace("\n", "").replace(" ", "");
        value = value.replace("\n", "");
        ItemM itemM = new ItemM(key, value);
        itemsUseCase.set(itemM);
    }

    public LiveData<List<ItemM>> getData() {
        if (data == null) {
            data = new MutableLiveData<>();
            itemsUseCase.getAll(items -> {
                isHaveRoot.set(!items.isEmpty());
                data.postValue(items);
            });
        }
        return data;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        adapter.set(null);
    }

    private void onItemClick(View view) {
        ItemVM itemVM = (ItemVM) view.getTag();
        showEditDialog.postValue(itemVM);
    }

    public void onItemChange(ItemVM itemVM, String newValue) {
        String oldValue = itemVM.value.get();
        newValue = newValue.replace("\n", "");
        boolean result = !TextUtils.equals(oldValue, newValue);
        if (result) {
            itemVM.value.set(newValue);
            itemsUseCase.set(itemVM.getItemM());
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        new MenuInflater(v.getContext()).inflate(R.menu.activity_main_context_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setActionView(v);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        View view = item.getActionView();
        ItemVM itemVM = (ItemVM) view.getTag();
        switch (item.getItemId()) {
            case R.id.edit:
                showEditDialog.postValue(itemVM);
                return true;
            case R.id.delete:
                itemsUseCase.delete(itemVM.getItemM());
                return true;
            default:
                return false;
        }
    }

    public boolean onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                saveAll();
                return true;
            case R.id.about:
                showAboutDialog.call();
                return true;
            default:
                return false;
        }
    }

    public boolean search(String searchStr) {
        ItemsAdapter adapter = this.adapter.get();
        if (adapter == null) {
            return true;
        }
        if (TextUtils.isEmpty(searchStr)) {
            adapter.setFilter(null);
        }
        adapter.setFilter(itemM -> itemM.getKey().contains(searchStr));
        return true;
    }

    private void saveAll() {
        if (itemsUseCase.commit()) {
            Toast.makeText(getApplication(), R.string.saved, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplication(), R.string.no_root, Toast.LENGTH_SHORT).show();
        }
    }
}
