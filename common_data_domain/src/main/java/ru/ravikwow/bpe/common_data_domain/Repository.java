package ru.ravikwow.bpe.common_data_domain;

import java.util.Map;

public interface Repository {
    void getAll(Callback callback);

    void set(String key, String value);

    void delete(String key);

    boolean commit();

    interface Callback {
        void send(Map<String, String> map);
    }
}
