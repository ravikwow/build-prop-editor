package ru.ravikwow.bpe.data;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by ravikwow
 * Date: 23.12.15
 */
public class RootUtils {
    private static final String suName = "su";

    public static boolean runCommmandsFromRoot(ArrayList<String> commands) {
        Process process = null;
        DataOutputStream os = null;
        int result = 1;
        try {
            process = Runtime.getRuntime().exec(suName);
            os = new DataOutputStream(process.getOutputStream());
            for (String command : commands) os.writeBytes(command + "\n");
            os.writeBytes("exit\n");
            os.flush();
            result = process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (os != null) os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (process != null) process.destroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result == 0;
    }

    public static ArrayList<String> readFileFromRoot(String path) {
        ArrayList<String> commands = new ArrayList<>();
        commands.add("cat " + path);
        Process process = null;
        DataOutputStream os = null;
        ArrayList<String> result = new ArrayList<>();
        try {
            process = Runtime.getRuntime().exec(suName);
            InputStream in = process.getInputStream();
            os = new DataOutputStream(process.getOutputStream());
            for (String command : commands) os.writeBytes(command + "\n");
            os.writeBytes("exit\n");
            os.flush();
            InputStreamReader istream_reader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(istream_reader);
            String line;
            while ((line = bufferedReader.readLine()) != null) result.add(line);
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (os != null) os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (process != null) process.destroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
