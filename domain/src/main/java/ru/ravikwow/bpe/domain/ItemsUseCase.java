package ru.ravikwow.bpe.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.ravikwow.bpe.common_data_domain.Repository;
import ru.ravikwow.bpe.common_domain_presentation.ItemM;

public class ItemsUseCase {
    private Repository repository;

    public ItemsUseCase(Repository repository) {
        this.repository = repository;
    }

    public void getAll(final Callback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                repository.getAll(new Repository.Callback() {
                    @Override
                    public void send(Map<String, String> map) {
                        if (callback == null) {
                            return;
                        }
                        List<ItemM> items = new ArrayList<>();
                        for (Map.Entry<String, String> entry : map.entrySet()) {
                            ItemM item = new ItemM(entry.getKey(), entry.getValue());
                            items.add(item);
                        }
                        callback.send(items);
                    }
                });
            }
        }).start();
    }

    public void set(ItemM itemM) {
        repository.set(itemM.getKey(), itemM.getValue());
    }

    public void delete(ItemM itemM) {
        repository.delete(itemM.getKey());
    }

    public boolean commit() {
        return repository.commit();
    }

    public interface Callback {
        void send(List<ItemM> items);
    }
}
