package ru.ravikwow.bpe.presentation.utils;

import android.view.View;

import androidx.databinding.BindingAdapter;

/**
 * Created by ravikwow
 * Date: 23.11.18.
 */
public class BindingAttributes {
    @BindingAdapter({"visibility"})
    public static void visibility(View view, Boolean isVisible) {
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }
}
