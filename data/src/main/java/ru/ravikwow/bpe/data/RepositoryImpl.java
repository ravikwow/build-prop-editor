package ru.ravikwow.bpe.data;

import android.content.Context;
import android.text.TextUtils;

import java.util.LinkedHashMap;
import java.util.Map;

import ru.ravikwow.bpe.common_data_domain.Repository;

/**
 * Created by ravikwow
 * Date: 21.11.18.
 */
public class RepositoryImpl implements Repository {
    private BuildPropEditor editor = new BuildPropEditor();
    private Context context;
    private Callback callback;

    public RepositoryImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getAll(Callback callback) {
        this.callback = callback;
        loadData();
    }

    @Override
    public void set(String key, String value) {
        editor.getValues(context);
        editor.setValue(key, value);
        loadData();
    }

    @Override
    public void delete(String key) {
        editor.getValues(context);
        editor.delValue(key);
        loadData();
    }

    @Override
    public boolean commit() {
        editor.getValues(context);
        return editor.commit(context);
    }

    private void loadData() {
        Map<String, String> result = new LinkedHashMap<>();
        for (int i = 0; i < editor.getValues(context).size(); i++) {
            String line = editor.getValues(context).get(i);
            if (line.startsWith("#") || TextUtils.isEmpty(line.trim())) {
                continue;
            }
            String[] strings = line.split("=");
            String key = strings.length > 0 ? strings[0] : null;
            if (key == null) {
                continue;
            }
            String value = strings.length > 1 ? strings[1] : "";
            result.put(key, value);
        }
        if (callback != null) {
            callback.send(result);
        }
    }
}
