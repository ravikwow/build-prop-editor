package ru.ravikwow.bpe.presentation.adapter;

import android.text.TextUtils;

import com.ravikwow.databinding.adapter.DiffRecyclerViewAdapter;
import com.ravikwow.databinding.adapter.RecyclerViewAdapter;

import ru.ravikwow.bpe.common_domain_presentation.ItemM;
import ru.ravikwow.bpe.presentation.R;
import ru.ravikwow.bpe.presentation.vm.ItemVM;

/**
 * Created by ravikwow
 * Date: 19.11.18.
 */
public class ItemsAdapter extends RecyclerViewAdapter<ItemM, ItemVM> {

    public ItemsAdapter() {
        super(new DiffRecyclerViewAdapter.CompareCallbacks<ItemM>() {
            @Override
            public boolean areItemsTheSame(ItemM itemM1, ItemM itemM2) {
                return TextUtils.equals(itemM1.getKey(), itemM2.getKey());
            }

            @Override
            public boolean areContentsTheSame(ItemM itemM1, ItemM itemM2) {
                return itemM1.equals(itemM2);
            }
        }, R.layout.view_item);
    }

    @Override
    protected void bindViewModel(ItemM itemM, ItemVM itemVM, int position) {
        itemVM.bind(itemM);
    }
}
