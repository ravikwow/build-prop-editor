package ru.ravikwow.bpe.data;

import android.content.Context;
import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created: ravikwow
 * Date: 22.12.15
 */
public class BuildPropEditor {
    private static final String pathBuildProp = "/system/build.prop";
    private ArrayList<String> mBuildPropData = null;

    private static ArrayList<String> readToEdit(Context context) {
        ArrayList<String> buildPropData = new ArrayList<>();
        try {
            String line;
            FileReader fileReader = new FileReader(pathBuildProp);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null) buildPropData.add(line);
        } catch (IOException e) {
            try {
                buildPropData.clear();
                String buildPropInData = context.getFilesDir().getAbsolutePath() + "/build.prop.system";
                if (copyToData(buildPropInData)) {
                    buildPropData.addAll(RootUtils.readFileFromRoot(buildPropInData));
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
        return buildPropData;
    }

    private static String writeToData(Context context, ArrayList<String> buildPropData) {
        String buildPropEditPath;
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        try {
            buildPropEditPath = context.getFilesDir().getAbsolutePath() + "/build.prop.edit";
            fileWriter = new FileWriter(buildPropEditPath);
            bufferedWriter = new BufferedWriter(fileWriter);
            for (String line : buildPropData) {
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (bufferedWriter != null) bufferedWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (fileWriter != null) fileWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return buildPropEditPath;
    }

    private static boolean copyToData(String pathBuildPropInData) {
        ArrayList<String> commands = new ArrayList<>();
        commands.add("rm " + pathBuildPropInData);
        commands.add("cp -f " + pathBuildProp + " " + pathBuildPropInData);
        commands.add("chmod 0644 " + pathBuildPropInData);
        return RootUtils.runCommmandsFromRoot(commands);
    }

    private static boolean moveToSystem(String buildPropEditPath) {
        ArrayList<String> commands = new ArrayList<>();
        commands.add("mount -o remount,rw /system");
        commands.add("cp -f " + pathBuildProp + " " + pathBuildProp + ".bak");
        commands.add("cp -f " + buildPropEditPath + " " + pathBuildProp);
        commands.add("chmod 0644 " + pathBuildProp);
        commands.add("chown root:root " + pathBuildProp);
        commands.add("rm -f " + buildPropEditPath);
        commands.add("mount -o remount,ro /system");
        return RootUtils.runCommmandsFromRoot(commands);
    }

    public ArrayList<String> getValues(Context context) {
        if (mBuildPropData == null) {
            mBuildPropData = readToEdit(context);
        }
        return mBuildPropData;
    }

    public BuildPropEditor setValue(String prop, String value) {
        if (TextUtils.isEmpty(prop)) {
            return this;
        }
        boolean isFinded = false;
        for (int i = 0; i < mBuildPropData.size(); i++) {
            String line = mBuildPropData.get(i);
            if (line.startsWith("#") || TextUtils.isEmpty(line.trim())) continue;
            String[] strings = line.split("=");
            if (strings.length == 0) continue;
            if (TextUtils.equals(strings[0].trim(), prop.trim())) {
                isFinded = true;
                mBuildPropData.set(i, prop + "=" + value);
            }
        }
        if (!isFinded) mBuildPropData.add(prop + "=" + value);
        return this;
    }

    public BuildPropEditor delValue(String prop) {
        if (TextUtils.isEmpty(prop)) {
            return this;
        }
        ArrayList<String> removeProps = new ArrayList<>();
        for (String line : mBuildPropData) {
            if (line.startsWith("#") || TextUtils.isEmpty(line.trim())) continue;
            String[] strings = line.split("=");
            if (strings.length == 0) continue;
            if (TextUtils.equals(strings[0].trim(), prop.trim())) {
                removeProps.add(line);
            }
        }
        for (String line : removeProps) {
            mBuildPropData.remove(line);
        }
        return this;
    }

    public boolean commit(Context context) {
        String buildPropEditPath = writeToData(context, mBuildPropData);
        return moveToSystem(buildPropEditPath);
    }
}
